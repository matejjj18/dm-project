package fer.dm.projekt.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TmdbMovieModel implements Serializable {

    public static final String TMDB_IMAGE_API_ENDPOINT = "https://image.tmdb.org/t/p/w500";

    @Id
    private String id;
    private String language;
    private String title;
    private String originalTitle;
    private String overview;
    private double popularity;
    private String releaseDate;
    private double voteAverage;
    private int voteCount;
    private String posterPath;
    private boolean isFavourite;
    private List<String> genreIds;
    private boolean adult;
    private String backdropPath;
    private String youtubeLink;

    public TmdbMovieModel() {
    }

    public TmdbMovieModel(OmdbMovieModel omdbMovieModel){
        this.language = omdbMovieModel.getLanguage();
        this.originalTitle = omdbMovieModel.getTitle();
        this.title = omdbMovieModel.getTitle();
        this.overview = omdbMovieModel.getPlot();
    }

    public TmdbMovieModel(String language, String title, String overview, double popularity, String releaseDate, double voteAverage, int voteCount, String posterPath, String originalTitle, List<String> genreIds, boolean adult) {
        this.language = language;
        this.title = title;
        this.overview = overview;
        this.popularity = popularity;
        this.releaseDate = releaseDate;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.originalTitle = originalTitle;
        this.genreIds = genreIds;
        this.adult = adult;
    }

    public TmdbMovieModel(String id, String language, String title, String overview, double popularity, String releaseDate, double voteAverage, int voteCount, String posterPath, String originalTitle) {
        this.id = id;
        this.language = language;
        this.title = title;
        this.overview = overview;
        this.popularity = popularity;
        this.releaseDate = releaseDate;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.originalTitle = originalTitle;
    }

    public String getId() {
        return id;
    }

    @JsonAlias({"id", "_id"})
    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    @JsonProperty("original_language")
    @JsonAlias({"language", "original_language"})
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @JsonProperty("release_date")
    @JsonAlias({"release_date", "releaseDate"})
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    @JsonProperty("vote_average")
    @JsonAlias({"voteAverage", "vote_average"})
    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public int getVoteCount() {
        return voteCount;
    }

    @JsonProperty("vote_count")
    @JsonAlias({"voteCount", "vote_count"})
    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    @JsonProperty("poster_path")
    @JsonAlias({"poster_path", "posterPath"})
    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @JsonProperty("original_title")
    @JsonAlias({"originalTitle", "original_title"})
    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    @JsonProperty("genre_ids")
    @JsonAlias({"genre_ids", "genreIds"})
    public List<String> getGenreIds() {
        return genreIds;
    }

    @JsonProperty("genre_ids")
    @JsonAlias({"genreIds", "genre_ids"})
    public void setGenreIds(List<String> genreIds) {
        this.genreIds = genreIds;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    @JsonProperty("backdrop_path")
    @JsonAlias({"backdrop_path", "backdropPath"})
    public String getBackdropPath() {
        return backdropPath;
    }
    @JsonProperty("backdrop_path")
    @JsonAlias({"backdrop_path", "backdropPath"})
    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    @Override
    public String toString() {
        return "MovieModel{" +
                "id='" + id + '\'' +
                ", language='" + language + '\'' +
                ", title='" + title + '\'' +
                ", overview='" + overview + '\'' +
                ", popularity=" + popularity +
                ", releaseDate='" + releaseDate + '\'' +
                ", voteAverage=" + voteAverage +
                ", voteCount=" + voteCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TmdbMovieModel that = (TmdbMovieModel) o;
        return Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}