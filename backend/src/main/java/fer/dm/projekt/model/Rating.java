package fer.dm.projekt.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Rating implements Serializable {

    @JsonProperty("Source")
    private String Source;

    @JsonProperty("Value")
    private String Value;

    public Rating(String source, String value) {
        Source = source;
        Value = value;
    }

    public Rating() {
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
