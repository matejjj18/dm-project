package fer.dm.projekt.model;

import com.restfb.types.User;
import org.springframework.data.annotation.Id;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class UserModel {

    @Id
    public String id;

    public String firstName;
    public String lastName;
    public String email;
    public String facebookKey;
    public HashSet<MovieModel> favouriteMovies;
    public String location;
    public List<MovieModel> initChosenMovies;
    public String gender;
    public String birthday;
    public List<String> facebookLikedMovies;


    public UserModel() {
    }

    public UserModel(String firstName, String lastName, String email, String facebookKey) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.facebookKey = facebookKey;
    }

    public UserModel(User user) {
        this.email = user.getEmail();
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.gender = user.getGender();
        this.birthday = user.getBirthday();
        if (user.getLocation() != null) {
            this.location = user.getLocation().getName();
        }
    }

    public UserModel(User user, String token) {
        this(user);
        this.facebookKey = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookKey() {
        return facebookKey;
    }

    public void setFacebookKey(String facebookKey) {
        this.facebookKey = facebookKey;
    }

    public void addFavouriteMovie(MovieModel movie) {
        if (favouriteMovies == null) {
            favouriteMovies = new HashSet<>();
        }
        if (!favouriteMovies.contains(movie)) {
            favouriteMovies.add(movie);
        }
    }

    public void removeFavouriteMovie(MovieModel movie) {
        if (favouriteMovies != null) {
            if (favouriteMovies.contains(movie)) {
                favouriteMovies.remove(movie);
            }
        }
    }

    public HashSet<MovieModel> getFavouriteMovies() {
        return favouriteMovies;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setFavouriteMovies(HashSet<MovieModel> favouriteMovies) {
        this.favouriteMovies = favouriteMovies;
    }

    public List<MovieModel> getInitChosenMovies() {
        return initChosenMovies;
    }

    public void setInitChosenMovies(List<MovieModel> initChosenMovies) {
        this.initChosenMovies = initChosenMovies;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }



    public boolean isAdult() {
        SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date = parser.parse(birthday);
            Date now = new Date();
            long millis = now.getTime() - date.getTime();
            if (millis < 1000L * 60L * 60L * 24L * 365L * 18L) {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    public int getAge() {
        SimpleDateFormat parser = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date = parser.parse(birthday);
            Date now = new Date();

            long timeBetween = now.getTime() - date.getTime();
            double yearsBetween = timeBetween / 3.15576e+10;
            return (int) Math.floor(yearsBetween);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 18;

    }

    public List<String> getFacebookLikedMovies() {
        return facebookLikedMovies;
    }

    public void setFacebookLikedMovies(List<String> facebookLikedMovies) {
        this.facebookLikedMovies = facebookLikedMovies;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
