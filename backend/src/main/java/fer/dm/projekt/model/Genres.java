package fer.dm.projekt.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Genres {
    public static List<String> allGenres = new LinkedList<>();
    static{
        allGenres.add("Action");
        allGenres.add("Adventure");
        allGenres.add("Animation");
        allGenres.add("Comedy");
        allGenres.add("Crime");
        allGenres.add("Documentary");
        allGenres.add("Drama");
        allGenres.add("Family");
        allGenres.add("Fantasy");
        allGenres.add("History");
        allGenres.add("Horror");
        allGenres.add("Music");
        allGenres.add("Mystery");
        allGenres.add("Romance");
        allGenres.add("Science Fiction");
        allGenres.add("TV Movie");
        allGenres.add("Triller");
        allGenres.add("War");
        allGenres.add("Western");
    }
}
