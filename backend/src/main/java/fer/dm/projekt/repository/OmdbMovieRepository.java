package fer.dm.projekt.repository;

import fer.dm.projekt.model.OmdbMovieModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OmdbMovieRepository extends MongoRepository<OmdbMovieModel, String> {
    public OmdbMovieModel findFirstByTitle(String name);
}