package fer.dm.projekt.repository;


import fer.dm.projekt.model.MovieModel;
import fer.dm.projekt.model.TmdbMovieModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TmdbMovieRepository extends MongoRepository<TmdbMovieModel, String> {
    public TmdbMovieModel findFirstByTitle(String name);
    public List<TmdbMovieModel> findTop30ByOrderByPopularityDesc();
    public TmdbMovieModel findFirstById(String id);
    public List<TmdbMovieModel> findTop20ByGenreIdsContainingOrderByPopularityDesc(List<String> genreIds);
    public List<TmdbMovieModel> findTop5ByOrderByPopularityDesc();

    @Query(" { title: { $regex : '(?i)?0'}  } }")
    List<TmdbMovieModel> findMoviesByRegexpName(String regexp);
}
