package fer.dm.projekt.security;

import com.restfb.*;
import com.restfb.json.JsonObject;
import com.restfb.types.User;
import fer.dm.projekt.model.UserModel;
import fer.dm.projekt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static java.lang.System.out;
import static java.lang.System.setOut;

@Component
public class CustomAuthenticationProvider
        implements AuthenticationProvider {

    @Autowired
    UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication auth)
            throws AuthenticationException {

        String token = auth.getName();

        String secretToken = "d55ad9bd99250db6e098cd0aaf0c3a1e";
        FacebookClient facebookClient = new DefaultFacebookClient(token, secretToken, Version.VERSION_2_8);

        try {
            User user = facebookClient.fetchObject("me", User.class, Parameter.with("fields", "name,email, first_name,last_name, gender, location, birthday, friends, likes"));
            Connection<User> friends = facebookClient.fetchConnection("me/friends", User.class, Parameter.with("fields", "name,email, first_name,last_name, gender, location, birthday, friends"));

            Connection<JsonObject> moviesConnection = facebookClient.fetchConnection("me/movies", JsonObject.class);

            List<String> userMovies = new LinkedList<>();
            for (List<JsonObject> moviesList : moviesConnection) {
                for (JsonObject movie : moviesList) {
                    userMovies.add(movie.get("name").toString());
                }
            }
            if (user.getId() != null) {
                UserModel existingUser = new UserModel(user, token);
                UserModel userFromDb = userRepository.findById(user.getId()).orElse(null);
                if (userFromDb != null) {
                    existingUser.setInitChosenMovies(userFromDb.getInitChosenMovies());
                    existingUser.setFavouriteMovies(userFromDb.getFavouriteMovies());
                }
                existingUser.setFacebookLikedMovies(userMovies);
                userRepository.save(existingUser);

                List<GrantedAuthority> authorities = new LinkedList<>();
                authorities.add(new SimpleGrantedAuthority("USER"));
                Authentication authUser = new UsernamePasswordAuthenticationToken
                        (existingUser, Collections.emptyList(), authorities);
                return authUser;
            }
        } catch (Exception e) {
            throw new BadCredentialsException("External system authentication failed");
        }
        throw new
                BadCredentialsException("External system authentication failed");

    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}