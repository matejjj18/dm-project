package fer.dm.projekt.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import fer.dm.projekt.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.token.Token;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * This class implements 2 methods:
 * <ul>
 * <li>attemptAuthentication</li>
 * <li>successfulAuthentication</li>
 * </ul>
 * Purpose of this object is to be injected in ldap authentication process. It gets login details from input stream of the request
 * and calls authenticate method.
 */
public class JWTAuthenticationFilter
        extends UsernamePasswordAuthenticationFilter {

    AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws AuthenticationException {
        try {
            LoginDetails loginDetails = new ObjectMapper().readValue(request.getInputStream(), LoginDetails.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDetails.getToken(), null));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
//        authResult.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authResult);
        ObjectMapper mapper = new ObjectMapper();
        //Object to JSON in String
        response.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_8));
        String jsonInString = mapper.writeValueAsString((UserModel)authResult.getPrincipal());
        response.getWriter().write(jsonInString);
    }
}
