package fer.dm.projekt.service.impl;

import com.restfb.json.JsonArray;
import com.restfb.json.JsonObject;
import com.restfb.json.JsonValue;
import fer.dm.projekt.model.*;
import fer.dm.projekt.repository.OmdbMovieRepository;
import fer.dm.projekt.repository.TmdbMovieRepository;
import fer.dm.projekt.repository.UserRepository;
import fer.dm.projekt.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    private final TmdbMovieRepository tmdbMovieRepository;

    private final UserRepository userRepository;

    private final OmdbMovieRepository omdbMovieRepository;

    @Autowired
    public MovieServiceImpl(TmdbMovieRepository tmdbMovieRepository, UserRepository userRepository, OmdbMovieRepository omdbMovieRepository) {
        this.tmdbMovieRepository = tmdbMovieRepository;
        this.userRepository = userRepository;
        this.omdbMovieRepository = omdbMovieRepository;
    }


    @Override
    public List<MovieModel> getMostPopularMovies() {
        List<TmdbMovieModel> tmdbMovieModelList = tmdbMovieRepository.findTop30ByOrderByPopularityDesc();
        List<MovieModel> movieModels = new LinkedList<>();
        tmdbMovieModelList.forEach(tmdbMovieModel -> {
            movieModels.add(getMovieByTitle(tmdbMovieModel.getTitle()));
        });
        return movieModels;
    }

    @Override
    public MovieModel getMovieByTitle(String title) {
        RestTemplate restTemplate = new RestTemplate();

        OmdbMovieModel omdbMovieModel = fetchOmdbMovieModel(title);

        TmdbMovieModel tmdbMovieModel = fetchTmdbMovieModel(title);

        if (tmdbMovieModel != null) {
            if (tmdbMovieModel.getYoutubeLink() == null) {
                String responseString = restTemplate.getForObject("https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyDWFuYY8krERgDVGSSqdoH46gJPWQADsys&q=" + title + " trailer", String.class);
                if (responseString != null) {
                    JsonObject jsonObject = JsonObject.readFrom(responseString);

                    int i = 0;
                    JsonValue val = jsonObject.get("items").asArray().get(i).asObject().get("id").asObject().get("videoId");
                    while (val == null) {
                        i++;
                        val = jsonObject.get("items").asArray().get(i).asObject().get("id").asObject().get("videoId");
                    }

                    String youtubeVideoId = val.toString();

                    String youtubeLink = "https://www.youtube.com/watch?v=" + youtubeVideoId.replaceAll("\"", "");
                    tmdbMovieModel.setYoutubeLink(youtubeLink);
                    tmdbMovieRepository.save(tmdbMovieModel);
                }
            }
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.add("Content-Type", "application/json");
        headers.add("trakt-api-key", "68c2aa268b8c6ea406a213c2ba22ab0312b9fff50247fdc6b6d711bb8f22cccb");
        headers.add("trakt-api-version", "2");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        int currentWatchCount = -1;
        if ((omdbMovieModel != null ? omdbMovieModel.getImdbID() : null) != null) {
            ResponseEntity<String> currentWatchList = null;
            try {
                currentWatchList = restTemplate.exchange("https://api.trakt.tv/movies/" + omdbMovieModel.getImdbID() + "/watching", HttpMethod.GET, entity, String.class);
                if (currentWatchList.getStatusCode().value() == 200) {
                    if (currentWatchList.getBody() != null) {
                        JsonArray jsonArray = JsonValue.readFrom(currentWatchList.getBody()).asArray();
                        currentWatchCount = jsonArray.size();
                    }
                }
            } catch (Exception e) {
                System.out.println("Trakt has a problem");
            }
        }
        int movieMentorRating = 0;

        return new MovieModel(omdbMovieModel, tmdbMovieModel, currentWatchCount, movieMentorRating);
    }

    private TmdbMovieModel fetchTmdbMovieModel(String title) {
        RestTemplate restTemplate = new RestTemplate();
        TmdbMovieModel tmdbMovieModel = tmdbMovieRepository.findFirstByTitle(title);
        if (tmdbMovieModel == null) {
            MoviesListModel moviesListModel = restTemplate.getForObject(
                    "https://api.themoviedb.org/3/search/movie?api_key=e5d18813ff278f1b763291f899639986&query=" + title,
                    MoviesListModel.class);
            tmdbMovieModel = (moviesListModel != null) ? (moviesListModel.getResults().size() > 0 ? moviesListModel.getResults().get(0) : null) : null;
            if (tmdbMovieModel != null) {
                tmdbMovieRepository.save(tmdbMovieModel);
            }
        }
        return tmdbMovieModel;
    }

    private OmdbMovieModel fetchOmdbMovieModel(String title) {
        RestTemplate restTemplate = new RestTemplate();
        OmdbMovieModel omdbMovieModel = omdbMovieRepository.findFirstByTitle(title);
        if (omdbMovieModel == null) {
            omdbMovieModel = restTemplate.getForObject("http://www.omdbapi.com/?t=" + title + "&apikey=f422361c", OmdbMovieModel.class);
            if (omdbMovieModel != null) {
                omdbMovieRepository.save(omdbMovieModel);
            }
        }
        return omdbMovieModel;
    }

    @Override
    public List<MovieModel> getMoviesForInitChoosingForUser(String userId) {
        UserModel userModel = userRepository.findById(userId).orElse(null);
        if (userModel == null) {
            return null;
        }


        HashMap<String, List<TmdbMovieModel>> genreMovies = new HashMap<>();

        for (String genre : Genres.allGenres) {
            List<String> genres = new LinkedList<>();
            genres.add(genre);
            genreMovies.put(genre, tmdbMovieRepository.findTop20ByGenreIdsContainingOrderByPopularityDesc(genres));
        }

        HashMap<String, Integer> movieWeightMap = new HashMap<>();

        HashMap<String, Integer> genreWeightMap = getWeightsForGenresByGender("female");
        HashMap<String, Integer> genderAgeWeightMap = getWeightsForGenresByAge(genreWeightMap, 15);

        int total = 0;
        for (Map.Entry<String, Integer> entry : genderAgeWeightMap.entrySet()) {
            total += entry.getValue();
        }
        for (Map.Entry<String, Integer> entry : genderAgeWeightMap.entrySet()) {
            entry.setValue((int) Math.ceil(Double.valueOf(entry.getValue()) / Double.valueOf(total) * 32));
        }

        List<MovieModel> movies = new LinkedList<>();

        for (Map.Entry<String, List<TmdbMovieModel>> entry : genreMovies.entrySet()) {
            int maxMoviesForThisGenre = genderAgeWeightMap.get(entry.getKey());
            for (int i = 0; i < entry.getValue().size() && i < maxMoviesForThisGenre; i++) {
                MovieModel movieModel = getMovieByTitle(entry.getValue().get(i).getTitle());
                if (!movies.contains(movieModel)) {
                    movies.add(movieModel);
                }
            }
        }
        return movies;
    }

    @Override
    public void addNewInitialChoosenMovies(String userId, List<SimpleMovieDto> movieModelList) {
        List<MovieModel> movieModels = new LinkedList<>();
        for (SimpleMovieDto simpleMovieDto : movieModelList) {
            movieModels.add(getMovieByTitle(simpleMovieDto.getTitle()));
        }
        addInitialChoosenMovies(userId, movieModels);
    }

    private HashMap<String, Integer> getMovieWeightMap(HashMap<String, List<TmdbMovieModel>> genreMovies, HashMap<String, Integer> movieWeightMap, HashMap<String, Integer> genreWeightMap) {
        for (Map.Entry<String, Integer> genreWeight : genreWeightMap.entrySet()) {
            List<TmdbMovieModel> movies = genreMovies.get(genreWeight.getKey());
            for (TmdbMovieModel movieModel : movies) {
                if (movieWeightMap.get(movieModel.getTitle()) != null) {
                    movieWeightMap.put(movieModel.getTitle(), 0);
                }
                int currentValue = movieWeightMap.get(movieModel.getTitle());
                movieWeightMap.put(movieModel.getTitle(), currentValue + genreWeight.getValue());
            }
        }
        return movieWeightMap;
    }

    private HashMap<String, Integer> getWeightsForGenresByAge(HashMap<String, Integer> allGenres, int age) {
        if (age < 12) {
            allGenres.compute("Action", ((key, value) -> (int) (value * 0.4)));
            allGenres.compute("Adventure", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("Animation", ((key, value) -> (int) (value * 3)));
            allGenres.compute("Comedy", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Crime", ((key, value) -> (int) (value * 0.2)));
            allGenres.compute("Documentary", ((key, value) -> (int) (value * 0.2)));
            allGenres.compute("Drama", ((key, value) -> (int) (value * 0.3)));
            allGenres.compute("Family", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("Fantasy", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("History", ((key, value) -> (int) (value * 0.4)));
            allGenres.compute("Horror", ((key, value) -> (int) (value * 0.0)));
            allGenres.compute("Music", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("Mystery", ((key, value) -> (int) (value * 0.1)));
            allGenres.compute("Romance", ((key, value) -> (int) (value * 0.4)));
            allGenres.compute("Science Fiction", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("TV Movie", ((key, value) -> (int) (value * 0.4)));
            allGenres.compute("Triller", ((key, value) -> (int) (value * 0.1)));
            allGenres.compute("War", ((key, value) -> (int) (value * 0.7)));
            allGenres.compute("Western", ((key, value) -> (int) (value * 0.8)));
        }

        if (age > 12 && age < 18) {
            allGenres.compute("Action", ((key, value) -> (int) (value * 0.6)));
            allGenres.compute("Adventure", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Animation", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("Comedy", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Crime", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Documentary", ((key, value) -> (int) (value * 0.6)));
            allGenres.compute("Drama", ((key, value) -> (int) (value * 0.6)));
            allGenres.compute("Family", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Fantasy", ((key, value) -> (int) (value * 1.1)));
            allGenres.compute("History", ((key, value) -> (int) (value * 0.7)));
            allGenres.compute("Horror", ((key, value) -> (int) (value * 0.5)));
            allGenres.compute("Music", ((key, value) -> (int) (value * 1.2)));
            allGenres.compute("Mystery", ((key, value) -> (int) (value * 0.5)));
            allGenres.compute("Romance", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Science Fiction", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("TV Movie", ((key, value) -> (int) (value * 0.4)));
            allGenres.compute("Triller", ((key, value) -> (int) (value * 0.5)));
            allGenres.compute("War", ((key, value) -> (int) (value * 0.8)));
            allGenres.compute("Western", ((key, value) -> (int) (value * 0.8)));
        }
        if (age > 18) {
            allGenres.compute("Action", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Adventure", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Animation", ((key, value) -> (int) (value * 0.5)));
            allGenres.compute("Comedy", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Crime", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Documentary", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Drama", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Family", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Fantasy", ((key, value) -> (int) (value * 1)));
            allGenres.compute("History", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Horror", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Music", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Mystery", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Romance", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Science Fiction", ((key, value) -> (int) (value * 1)));
            allGenres.compute("TV Movie", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Triller", ((key, value) -> (int) (value * 1)));
            allGenres.compute("War", ((key, value) -> (int) (value * 1)));
            allGenres.compute("Western", ((key, value) -> (int) (value * 1)));
        }
        return allGenres;
    }

    private HashMap<String, Integer> getWeightsForGenresByGender(String gender) {
        HashMap<String, Integer> allGenres = new HashMap<>();
        if (gender.toLowerCase().equals("female")) {
            allGenres.put("Action", 86);
            allGenres.put("Adventure", 89);
            allGenres.put("Animation", 75);
            allGenres.put("Comedy", 91);
            allGenres.put("Crime", 84);
            allGenres.put("Documentary", 77);
            allGenres.put("Drama", 89 + 15);
            allGenres.put("Family", 85);
            allGenres.put("Fantasy", 70);
            allGenres.put("History", 65);
            allGenres.put("Horror", 47);
            allGenres.put("Music", 64);
            allGenres.put("Mystery", 83);
            allGenres.put("Romance", 77 + 15);
            allGenres.put("Science Fiction", 55);
            allGenres.put("TV Movie", 10);
            allGenres.put("Triller", 83);
            allGenres.put("War", 32);
            allGenres.put("Western", 40);
        } else {
            allGenres.put("Action", 90 + 15);
            allGenres.put("Adventure", 90 + 15);
            allGenres.put("Animation", 65);
            allGenres.put("Comedy", 90);
            allGenres.put("Crime", 79);
            allGenres.put("Documentary", 78);
            allGenres.put("Drama", 80);
            allGenres.put("Family", 70);
            allGenres.put("Fantasy", 71 + 15);
            allGenres.put("History", 70);
            allGenres.put("Horror", 57 + 15);
            allGenres.put("Music", 48);
            allGenres.put("Mystery", 84);
            allGenres.put("Romance", 55);
            allGenres.put("Science Fiction", 76 + 10);
            allGenres.put("TV Movie", 10);
            allGenres.put("Triller", 84);
            allGenres.put("War", 55 + 10);
            allGenres.put("Western", 55 + 10);
        }
        return allGenres;
    }

    @Override
    public void addInitialChoosenMovies(String userId, List<MovieModel> movieList) {
        UserModel userModel = userRepository.findById(userId).orElse(null);
        if (userModel != null) {
            userModel.setInitChosenMovies(movieList);
            userRepository.save(userModel);
        }
    }

    /**
     * This recommender uses different services to recommend movies for user.
     * Every movie gets its MovieMentorRating, a coefficient which tells how important a movie is -
     * the bigger the coefficient, the higher a movie will be placed on recommendation list.
     *
     * @param userId
     * @return
     */
    @Override
    public List<MovieModel> getRecommendedMoviesForUser(String userId) {

        UserModel user = userRepository.findById(userId).orElse(null);
        Map<String, Integer> recommendations = new HashMap<>();

        List<String> facebookMovies = user.getFacebookLikedMovies();
        List<MovieModel> initialChosenMovies = user.getInitChosenMovies();
//        List<MovieModel> initialChosenMovies = new LinkedList<>();
//        List<TmdbMovieModel> tmdbMovies = tmdbMovieRepository.findTop5ByOrderByPopularityDesc();
//        tmdbMovies.forEach(movie -> initialChosenMovies.add(getMovieByTitle(movie.getTitle())));

        System.out.println("tasteDiveFacebook......");
        List<String> tasteDiveBasedOnFacebook = new LinkedList<>();
        facebookMovies.forEach(movie -> {
            List<String> results = getSimilarMoviesFromTasteDive(movie);
            results.forEach(r -> tasteDiveBasedOnFacebook.add(r));
        });
        putMoviesInMap(recommendations, tasteDiveBasedOnFacebook, 50);

        System.out.println("tasteDiveChosen......");
        List<String> tasteDiveBasedOnChosen = new LinkedList<>();
        initialChosenMovies.forEach(movie -> {
            List<String> results = getSimilarMoviesFromTasteDive(movie.getTitle());
            results.forEach(r -> tasteDiveBasedOnChosen.add(r));
        });
        putMoviesInMap(recommendations, tasteDiveBasedOnChosen, 10);

        System.out.println("tmdbChosen.......");
        List<String> tmdbBasedOnChosen = new LinkedList<>();
        initialChosenMovies.forEach(movie -> {
            List<String> results = getSimilarMoviesFromTmdb(movie.getTitle());
            results.forEach(r -> tmdbBasedOnChosen.add(r));
        });
        putMoviesInMap(recommendations, tmdbBasedOnChosen, 20);

        System.out.println("traktChosen.........");
        List<String> traktBasedOnChosen = new LinkedList<>();
        initialChosenMovies.forEach(movie -> {
            List<String> results = getSimilarMoviesFromTrakt(movie.getTitle());
            results.forEach(r -> traktBasedOnChosen.add(r));
        });
        putMoviesInMap(recommendations, traktBasedOnChosen, 15);

        System.out.println("traktActors1.........");
        String actor1 = initialChosenMovies.get(0).getActors().get(0);
        List<String> traktBasedOnChosenActors1 = getMoviesByActor(actor1);
        putMoviesInMap(recommendations, traktBasedOnChosenActors1, 5);

        System.out.println("traktActors2.........");
        String actor2 = initialChosenMovies.get(1).getActors().get(0);
        List<String> traktBasedOnChosenActors2 = getMoviesByActor(actor2);
        putMoviesInMap(recommendations, traktBasedOnChosenActors2, 5);


        Map<String, Integer> sortedByCount = recommendations.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

//        System.out.println("recommendations.........");
//        sortedByCount.keySet().forEach(key -> {
//            System.out.println(key + ": " + sortedByCount.get(key));
//        });

        Set<String> mapValues = sortedByCount.keySet();
        List<String> top15 = new LinkedList<>();

        int i = 0;
        Iterator<String> it = mapValues.iterator();
        while (i < 15 && it.hasNext()) {
            top15.add(it.next());
            i++;
        }

        List<MovieModel> results = new LinkedList<>();
        top15.forEach(title -> results.add(getMovieByTitle(title)));


        return results;
    }

    public void putMoviesInMap(Map<String, Integer> map, List<String> list, int weight) {
        list.forEach(movie -> {
            if (!map.containsKey(movie)) {
                map.put(movie, weight);
            } else {
                int oldValue = map.get(movie);
                map.put(movie, oldValue + weight);
            }
        });
    }

    public OmdbMovieModel getMovieDetailsFromOMDB(String title) {
        title = title.trim().replaceAll(" ", "+");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://www.omdbapi.com/?t=" + title + "&apikey=f422361c", OmdbMovieModel.class);
    }


    public List<String> getMoviesByActor(String actorName) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.add("Content-Type", "application/json");
        headers.add("trakt-api-key", "68c2aa268b8c6ea406a213c2ba22ab0312b9fff50247fdc6b6d711bb8f22cccb");
        headers.add("trakt-api-version", "2");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> res = null;
        List<String> movies = new LinkedList<>();
        try{

            res = restTemplate.exchange("https://api.trakt.tv/people/" + actorName.replaceAll(" ", "-").toLowerCase() + "/movies", HttpMethod.GET, entity, String.class);

            if (res.getStatusCode().value() == 200) {
                if (res.getBody() != null) {
                    JsonObject responseString = JsonObject.readFrom(res.getBody());
                    JsonArray jsonArray = responseString.get("cast").asArray();
                    jsonArray.forEach(entry -> {
                        JsonObject movie = entry.asObject().get("movie").asObject();
                        String movieName = movie.get("title").asString();
                        if (movieName != null) {
                            movies.add(movieName);
                        }
                    });
                }
            }
        } catch(Exception e){
            System.out.println("Error fetching movies by actor");
            return movies;
        }


        return movies;
    }

    @Override
    public Set<MovieModel> addToFavourites(String userId, MovieModel movieModel) {
        UserModel userModel = userRepository.findById(userId).orElse(null);
        if (userModel != null) {
            userModel.addFavouriteMovie(movieModel);
            userRepository.save(userModel);
            return userModel.getFavouriteMovies();
        }
        return new HashSet<>();

    }

    @Override
    public Set<MovieModel> removeFromFavourites(String userId, MovieModel movieModel) {
        UserModel userModel = userRepository.findById(userId).orElse(null);
        if (userModel != null) {
            userModel.removeFavouriteMovie(movieModel);
            userRepository.save(userModel);
            return userModel.getFavouriteMovies();
        }
        return new HashSet<>();

    }

    @Override
    public Set<MovieModel> getFavourites(String userId) {
        UserModel userModel = userRepository.findById(userId).orElse(null);
        if (userModel != null) {
            if (userModel.getFavouriteMovies() != null) {
                return userModel.getFavouriteMovies();
            }
        }
        return new HashSet<>();
    }

    @Override
    public List<SimpleMovieDto> getAutocompleteMovies(String query) {
        List<TmdbMovieModel> movies = tmdbMovieRepository.findMoviesByRegexpName(query);
        int counter = 0;
        List<SimpleMovieDto> simpleMovieDtos = new LinkedList<>();
        for(TmdbMovieModel movie : movies){
            SimpleMovieDto simpleMovieDto = new SimpleMovieDto(movie);
            simpleMovieDtos.add(simpleMovieDto);
            if(counter >5) break;
            counter ++;
        }
        return simpleMovieDtos;
    }

    @Override
    public List<MovieModel> getMoviesByDirector(String actorName) {
        RestTemplate restTemplate = new RestTemplate();
        String res = restTemplate.getForObject(
                "https://api.trakt.tv/people/" + actorName + "/movies",
                String.class);
        List<MovieModel> movieModels = new LinkedList<>();

        if (res != null) {
            JsonObject response = JsonObject.readFrom(res);
            JsonArray jsonArray = response.get("crew").asObject().get("production").asArray();
            jsonArray.forEach(entry -> {
                String job = entry.asObject().getString("job", "");
                if (job.equals("Director")) {
                    JsonObject movie = entry.asObject().get("movie").asObject();
                    String movieName = movie.get("title").toString().replaceAll("\"", "");
                    MovieModel movieModel = getMovieByTitle(movieName);
                    if (movieModel != null) {
                        movieModels.add(movieModel);
                    }
                }
            });
        }

        return movieModels;
    }

    public List<String> getSimilarMoviesFromTasteDive(String title) {
        // https://tastedive.com/api/similar?k=327661-MovieMen-IV2557Z2&type=movies&q=the+lord+of+the+rings+trilogy
        if(title == null) {
            return new LinkedList<>();
        }
        title = title.trim().replaceAll("\\s+", "+").toLowerCase();
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> similarMoviesResponse = restTemplate.exchange("https://tastedive.com/api/similar?k=327661-MovieMen-IV2557Z2&type=movies&q=" + title, HttpMethod.GET, entity, String.class);
        List<String> movies = new LinkedList<>();
        try {
            if (similarMoviesResponse.getStatusCode().value() == 200) {
                if (similarMoviesResponse.getBody() != null) {
                    JsonObject jsonObject = JsonObject.readFrom(similarMoviesResponse.getBody());
                    JsonArray arrayOfJsonObjects = jsonObject.get("Similar").asObject().get("Results").asArray();

                    arrayOfJsonObjects.forEach(jsonValue -> {
                        String movieTitle = jsonValue.asObject().getString("Name", null);
                        if (movieTitle != null) {
                            movies.add(movieTitle);
                        }
                    });
                }
            }
        } catch (Exception e){
            System.out.println("Error loading from taste dive");
        }

        return movies;
    }

    public List<String> getSimilarMoviesFromTmdb(String title) {
        TmdbMovieModel tmdbMovieModel = fetchTmdbMovieModel(title);

        RestTemplate restTemplate = new RestTemplate();
        List<String> similarMoviesFromTmdb = new LinkedList<>();
        try{

            MoviesListModel moviesListModel = restTemplate.getForObject(
                    "https://api.themoviedb.org/3/movie/" + tmdbMovieModel.getId() + "/recommendations?api_key=e5d18813ff278f1b763291f899639986",
                    MoviesListModel.class);
            moviesListModel.getResults().forEach(tmdbMovie -> similarMoviesFromTmdb.add(tmdbMovie.getTitle()));

        } catch (Exception e){
            System.out.println("Error fetching tmdb movies");
        }

        return similarMoviesFromTmdb;
    }

    public List<String> getSimilarMoviesFromTrakt(String title) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.add("Content-Type", "application/json");
        headers.add("trakt-api-key", "68c2aa268b8c6ea406a213c2ba22ab0312b9fff50247fdc6b6d711bb8f22cccb");
        headers.add("trakt-api-version", "2");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        List<String> similarMoviesFromTrakt = new LinkedList<>();

        RestTemplate restTemplate = new RestTemplate();
        OmdbMovieModel omdbMovieModel = fetchOmdbMovieModel(title);
        if ((omdbMovieModel != null ? omdbMovieModel.getImdbID() : null) != null) {
            ResponseEntity<String> relatedMovies = restTemplate.exchange("https://api.trakt.tv/movies/" + omdbMovieModel.getImdbID() + "/related", HttpMethod.GET, entity, String.class);

            if (relatedMovies.getStatusCode().value() == 200) {
                if (relatedMovies.getBody() != null) {
                    JsonArray jsonArray = JsonValue.readFrom(relatedMovies.getBody()).asArray();
                    jsonArray.forEach(movie -> {
                        similarMoviesFromTrakt.add(movie.asObject().get("title").asString());
                    });
                }
            }
        }

        return similarMoviesFromTrakt;
    }

    @Override
    public List<TweetModel> getLastTweetsforMovieId(String movieId) {
        RestTemplate restTemplate = new RestTemplate();

        String title = tmdbMovieRepository.findFirstById(movieId).getTitle() + " movie";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer AAAAAAAAAAAAAAAAAAAAAA7p9AAAAAAAS7Vs%2Ff2EKPYE%2B%2BjLq4zfxZbBucY%3DPr1ejzzcIWTLPPDxRnB1VV0JnkqKgXStaMZuYDof2Lb2Y3S1QP");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> responseString = restTemplate.exchange("https://api.twitter.com/1.1/search/tweets.json?result_type=popular&q=" + title.toLowerCase(), HttpMethod.GET, entity, String.class);
        List<TweetModel> tweetModels = new LinkedList<>();
        if (responseString.getStatusCode().value() == 200) {
            if (responseString.getBody() != null) {
                JsonObject jsonObject = JsonObject.readFrom(responseString.getBody());
                JsonArray statuses = jsonObject.get("statuses").asArray();
                int count = 0;
                for (JsonValue status : statuses) {
                    String createdAt = status.asObject().get("created_at").asString();
                    String text = status.asObject().get("text").asString();
                    String user = status.asObject().get("user").asObject().get("screen_name").asString();

                    SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
                    Date date = null;
                    try {
                        date = format.parse(createdAt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    String newDateFormat = newFormat.format(date);
                    tweetModels.add(new TweetModel(newDateFormat, text, user));
                    count++;
                    if (count == 20) {
                        break;
                    }
                }
            }
        }
        return tweetModels;
    }
}
