package fer.dm.projekt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fer.dm.projekt.model.TmdbMovieModel;
import fer.dm.projekt.repository.TmdbMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.*;

@Service
public class ImportService {

    @Autowired
    TmdbMovieRepository tmdbMovieRepository;

    public void importMoviesFromJsonFile() throws IOException {
        if(tmdbMovieRepository.count() == 0){
            System.out.println("Loading tmdbMovies from tmdbMovieModel.json............ ");
            System.out.println("Please wait....");
            File file = ResourceUtils.getFile("classpath:tmdbMovieModel.json");
            ObjectMapper mapper = new ObjectMapper();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                TmdbMovieModel tmdbMovie = mapper.readValue(line, TmdbMovieModel.class);
                tmdbMovieRepository.save(tmdbMovie);
            }
            System.out.println("Total movies in db: " + tmdbMovieRepository.count());
        }
    }
}
