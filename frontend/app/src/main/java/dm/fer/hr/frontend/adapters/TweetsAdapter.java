package dm.fer.hr.frontend.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import dm.fer.hr.frontend.R;
import io.swagger.client.model.TweetModel;

public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.TweetViewHolder> {

    private List<TweetModel> tweets;

    private LayoutInflater mInflater;

    public TweetsAdapter(Context context, List<TweetModel> tweets) {
        this.mInflater = LayoutInflater.from(context);
        this.tweets = tweets;
    }

    @NonNull
    @Override
    public TweetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_tweet, viewGroup, false);
        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        TweetModel tweet = tweets.get(position);
        holder.updateView(tweet);
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    class TweetViewHolder extends RecyclerView.ViewHolder {

        private TextView userTextView;
        private TextView createdAtTextView;
        private TextView textTextView;

        private TweetViewHolder(View view) {
            super(view);

            userTextView = (TextView) view.findViewById(R.id.user_tv);
            createdAtTextView = (TextView) view.findViewById(R.id.created_tv);
            textTextView = (TextView) view.findViewById(R.id.text_tv);
        }

        protected void updateView(TweetModel tweet) {
            userTextView.setText(tweet.getUser());
            createdAtTextView.setText(tweet.getCreatedAt());
            textTextView.setText(tweet.getText());
        }

    }
}
