package dm.fer.hr.frontend.utils;

import android.view.animation.Interpolator;

public class CustomSpringInterpolator implements Interpolator {

    private float factor = 0.2f;

    public CustomSpringInterpolator() {}

    public CustomSpringInterpolator(float factor) {
        this.factor = factor;
    }

    @Override
    public float getInterpolation(float input) {
        return (float) (Math.pow(2, (-10 * input)) * Math.sin(((2* Math.PI) * (input - (factor/4)))/factor) + 1);
    }
}