package dm.fer.hr.frontend.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.OnItemSelectedListener;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.TabbedActivity;
import dm.fer.hr.frontend.adapters.PickMoviesAdapter;
import dm.fer.hr.frontend.utils.DestroyListener;
import dm.fer.hr.frontend.utils.SharedObjects;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.UserModel;

import static com.facebook.FacebookSdk.getApplicationContext;

public class PickMoviesDialog extends FullscreenDialog implements OnItemSelectedListener<MovieModel>, View.OnClickListener {

    private static final int MIN_MOVIES = 10;

    private RecyclerView recyclerView;
    private FloatingActionButton sendButton;
    private TextView fabTextView;

    private UserModel user;
    private List<MovieModel> selectedMovies;

    private DestroyListener destroyListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_pick_movies, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        selectedMovies = new ArrayList<>();

        initView(view);
        loadMovies();
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.movies_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        sendButton = (FloatingActionButton) view.findViewById(R.id.send_fab);
        fabTextView = (TextView) view.findViewById(R.id.num_tv);

        sendButton.setOnClickListener(this);
        updateButton();
    }

    private void updateView(List<MovieModel> movies) {
        PickMoviesAdapter adapter = new PickMoviesAdapter(getContext(), movies);
        adapter.setOnItemSelectedListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void loadMovies() {
        SharedObjects<UserModel> sharedObjects = new SharedObjects<UserModel>(getContext());
        user = sharedObjects.retrive(Constants.user, UserModel.class);

        MovieApi movieApi = new MovieApi();
        try {
            movieApi.apiMovieInitChoosingUserIdGetAsync(user.getId(), new SimpleCallback<List<MovieModel>>(getFragmentManager()) {
                @Override
                public void onSuccess(List<MovieModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        updateView(result);
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDialogBackPressed() {
        //do nothing
    }

    @Override
    public void onSelect(MovieModel item, boolean isSelected) {
        if (isSelected) {
            selectedMovies.add(item);
        } else {
            selectedMovies.remove(item);
        }

        updateButton();
    }

    private void updateButton() {
        fabTextView.setText(selectedMovies.size() + "/" + MIN_MOVIES);
        sendButton.setEnabled(selectedMovies.size() >= MIN_MOVIES);
    }

    @Override
    public void onClick(View v) {
        MovieApi movieApi = new MovieApi();
        try {
            movieApi.apiMovieInitChoosingUserIdPostAsync(user.getId(), selectedMovies, new SimpleCallback<Void>(getFragmentManager()) {
                @Override
                public void onSuccess(Void result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        SharedObjects sharedObjectBoolean = new SharedObjects<Boolean>(getApplicationContext());
                        sharedObjectBoolean.put(Constants.pickedInitMovies, true);
                        Intent intent = new Intent(getApplicationContext(), TabbedActivity.class);
                        startActivity(intent);
                        PickMoviesDialog.super.onDialogBackPressed();
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    public void setOnDestroyListenr(DestroyListener destroyListener) {
        this.destroyListener = destroyListener;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (destroyListener != null) {
            destroyListener.onDestroy();
        }
    }
}
