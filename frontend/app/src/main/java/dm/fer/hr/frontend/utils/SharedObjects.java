package dm.fer.hr.frontend.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class SharedObjects<T> {

    private SharedPreferences sharedPreferences;
    private Gson gson;

    public SharedObjects(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        gson = new Gson();
    }

    public void put(String key, T value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, gson.toJson(value));
        editor.apply();
    }

    public T retrive(String key, Class<T> k) {
        String value = sharedPreferences.getString(key, "");
        return gson.fromJson(value, k);
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }
}
