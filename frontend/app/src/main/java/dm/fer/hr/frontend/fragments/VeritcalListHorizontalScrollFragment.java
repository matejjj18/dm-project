package dm.fer.hr.frontend.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.adapters.MovieIconAdapter;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.model.MovieModel;

public class VeritcalListHorizontalScrollFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vertiacl_list_horizontal_scroll_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MovieApi movieApi = new MovieApi();
        try {
            movieApi.apiMoviePopularityGetAsync(new SimpleCallback<List<MovieModel>>(getFragmentManager()) {
                @Override
                public void onSuccess(final List<MovieModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        fillData(result, view);
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private void fillData(List<MovieModel> movies, View rootView) {
        LinearLayout verticalList = rootView.findViewById(R.id.verticalList);

        //sorting movies by genre
        Map<String, List<MovieModel>> moviesByGenres = new HashMap<>();
        for (MovieModel movie : movies) {
            if (movie.getGenres() != null) {
                for (String genre : movie.getGenres()) {
                    List<MovieModel> genreMovies = moviesByGenres.get(genre);
                    if (genreMovies == null) {
                        List<MovieModel> list = new ArrayList<>();
                        list.add(movie);
                        moviesByGenres.put(genre, list);
                    } else {
                        genreMovies.add(movie);
                    }
                }
            }
        }

        List<String> genresSorted = new ArrayList<>(moviesByGenres.keySet());
        Collections.sort(genresSorted);
        for (String genre : genresSorted) {
            View horizontalListLayout = LayoutInflater.from(getContext()).inflate(R.layout.layout_horizontal_list, (ViewGroup) rootView, false);
            RecyclerView horizontalList = horizontalListLayout.findViewById(R.id.horizontal_list);
            TextView genreTitle = horizontalListLayout.findViewById(R.id.genre_text);
            genreTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_1));
            genreTitle.setTypeface(genreTitle.getTypeface(), Typeface.ITALIC);
            genreTitle.setTextColor(Color.WHITE);
            genreTitle.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            genreTitle.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_1),
                    getResources().getDimensionPixelSize(R.dimen.padding_2),0,0);
            genreTitle.setText(genre);

            List<MovieModel> genreMovies = moviesByGenres.get(genre);
            MovieIconAdapter adapter = new MovieIconAdapter(genreMovies);
            adapter.setContext(getActivity());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            horizontalList.setLayoutManager(linearLayoutManager);

            horizontalList.setAdapter(adapter);
            verticalList.addView(horizontalListLayout);
        }
    }
}
