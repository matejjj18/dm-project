package dm.fer.hr.frontend.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import dm.fer.hr.frontend.OnItemSelectedListener;
import dm.fer.hr.frontend.R;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.SimpleMovieDto;

public class SimpleMovieAdapter extends RecyclerView.Adapter<SimpleMovieAdapter.SimpleMovieViewHolder> {

    private List<SimpleMovieDto> movies;
    private OnItemSelectedListener<SimpleMovieDto> onItemSelectedListener;
    private LayoutInflater mInflater;

    public SimpleMovieAdapter(Context context, List<SimpleMovieDto> movies) {
        this.mInflater = LayoutInflater.from(context);
        this.movies = movies;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener<SimpleMovieDto> onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    @NonNull
    @Override
    public SimpleMovieAdapter.SimpleMovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_simple_movie, viewGroup, false);
        return new SimpleMovieAdapter.SimpleMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleMovieAdapter.SimpleMovieViewHolder holder, int position) {
        SimpleMovieDto movie = movies.get(position);
        holder.updateView(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class SimpleMovieViewHolder extends RecyclerView.ViewHolder {
        private TextView movieTextView;

        private SimpleMovieViewHolder(View view) {
            super(view);
            movieTextView = view.findViewById(R.id.movie_tv);
        }

        protected void updateView(SimpleMovieDto movie) {
            movieTextView.setText(movie.getTitle() + " (" + movie.getYear() + ")");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemSelectedListener != null) {
                        onItemSelectedListener.onSelect(movie, true);
                    }
                }
            });
        }

    }
}
