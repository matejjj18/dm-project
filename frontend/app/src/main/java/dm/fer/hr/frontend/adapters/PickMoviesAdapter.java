package dm.fer.hr.frontend.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.OnItemSelectedListener;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.utils.CommonUtils;
import dm.fer.hr.frontend.utils.LoaderUtil;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.model.MovieModel;

public class PickMoviesAdapter extends RecyclerView.Adapter<PickMoviesAdapter.MovieViewHolder> {

    private List<MovieModel> movies;
    private List<Boolean> selections;
    private HashMap<String, Bitmap> movieIcons;
    private OnItemSelectedListener<MovieModel> onItemSelectedListener;
    private LayoutInflater mInflater;

    public PickMoviesAdapter(Context context, List<MovieModel> movies) {
        this.mInflater = LayoutInflater.from(context);
        this.movies = movies;

        selections = new ArrayList<>();
        for(int i = 0; i < movies.size(); i++) {
            selections.add(false);
        }

        this.movieIcons = new HashMap<>();
        loadMovieIcons(movies);
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_pick_movie, viewGroup, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        MovieModel movie = movies.get(position);
        holder.updateView(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setOnItemSelectedListener(OnItemSelectedListener<MovieModel> onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    private void loadMovieIcons(List<MovieModel> movies) {
        LoaderUtil loader = new LoaderUtil();
        loader.loadPosterImagesFromUrls(movies, new SimpleCallback<HashMap<String, Bitmap>>() {
            @Override
            public void onSuccess(HashMap<String, Bitmap> result, int statusCode, Map<String, List<String>> responseHeaders) {
                super.onSuccess(result, statusCode, responseHeaders);
                movieIcons.putAll(result);
                notifyDataSetChanged();
            }
        });
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private MovieModel movie;

        private LinearLayout itemLayout;
        private ImageView posterImageView;
        private TextView titleTextView;
        private TextView genresTextView;
        private TextView voteTextView;

        private MovieViewHolder(View view) {
            super(view);

            this.itemLayout = (LinearLayout) view.findViewById(R.id.item_layout);
            this.posterImageView = (ImageView) view.findViewById(R.id.poster_iv);
            this.titleTextView = (TextView) view.findViewById(R.id.title_tv);
            this.genresTextView = (TextView) view.findViewById(R.id.genres_tv);
            this.voteTextView = (TextView) view.findViewById(R.id.vote_tv);

            view.setOnClickListener(this);
        }

        protected void updateView(MovieModel movie) {
            this.movie = movie;
            titleTextView.setText(movie.getTitle() != null ? movie.getTitle() : "-");
            genresTextView.setText(CommonUtils.stringListToString(movie.getGenres()));
            voteTextView.setText(movie.getImdbRating() != null ? movie.getImdbRating()+"" : "-");

            if (movieIcons.keySet().contains(movie.getTmdbId() + movie.getId())
                    && movieIcons.get(movie.getTmdbId() + movie.getId()) != null) {
                posterImageView.setImageBitmap(movieIcons.get(movie.getTmdbId() + movie.getId()));
            } else {
                posterImageView.setImageDrawable(itemView.getResources().getDrawable(R.mipmap.ic_launcher));
            }

            updateSelection();
        }

        protected void updateSelection() {
            int position = movies.indexOf(movie);
            itemView.setSelected(selections.get(position));
            Resources resources = itemView.getContext().getResources();
            itemLayout.setBackgroundColor(selections.get(position) ? resources.getColor(R.color.colorAccent) : resources.getColor(R.color.white));
        }

        @Override
        public void onClick(View v) {
            int position = movies.indexOf(movie);
            selections.set(position, !selections.get(position));
            updateSelection();

            if(onItemSelectedListener != null) {
                onItemSelectedListener.onSelect(movie, selections.get(position));
            }
        }
    }
}
