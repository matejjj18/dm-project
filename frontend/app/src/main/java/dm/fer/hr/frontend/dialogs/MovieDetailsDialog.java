package dm.fer.hr.frontend.dialogs;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.adapters.TweetsAdapter;
import dm.fer.hr.frontend.utils.CommonUtils;
import dm.fer.hr.frontend.utils.LoaderUtil;
import dm.fer.hr.frontend.utils.SharedObjects;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.api.UserApi;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.TweetModel;
import io.swagger.client.model.UserModel;

public class MovieDetailsDialog extends FullscreenDialog {

    private MovieModel movie;

    private ImageView coverImageView;
    private ImageView posterImageView;
    private ImageView adultImageView;
    private ImageButton youtubeImageButton;
    private TextView titleTextView;
    private TextView yearTextView;
    private ImageButton favouriteImageButton;
    private TextView genresTextView;
    private TextView runtimeTextView;
    private TextView plotTextView;
    private TextView imdbTextView;
    private TextView tmdbTextView;
    private TextView rottenTomatoesTextView;
    private TextView metacriticsTextView;
    private TextView traktWatchingTextView;
    private TextView directorTextView;
    private TextView actorsTextView;
    private RecyclerView tweetsRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_movie_details, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        updateView();
        loadTweets();
    }

    public void setMovie(MovieModel movie) {
        this.movie = movie;
    }

    private void initView(View view) {
        coverImageView = (ImageView) view.findViewById(R.id.cover_iv);
        posterImageView = (ImageView) view.findViewById(R.id.poster_iv);
        adultImageView = (ImageView) view.findViewById(R.id.adult_iv);
        youtubeImageButton = (ImageButton) view.findViewById(R.id.youtube_btn);
        titleTextView = (TextView) view.findViewById(R.id.title_tv);
        yearTextView = (TextView) view.findViewById(R.id.year_tv);
        favouriteImageButton = (ImageButton) view.findViewById(R.id.favourites_btn);
        genresTextView = (TextView) view.findViewById(R.id.genres_tv);
        runtimeTextView = (TextView) view.findViewById(R.id.runtime_tv);
        plotTextView = (TextView) view.findViewById(R.id.plot_tv);
        imdbTextView = (TextView) view.findViewById(R.id.imdb_rating_tv);
        tmdbTextView = (TextView) view.findViewById(R.id.tmdb_rating_tv);
        rottenTomatoesTextView = (TextView) view.findViewById(R.id.rt_rating_tv);
        metacriticsTextView = (TextView) view.findViewById(R.id.mc_rating_tv);
        traktWatchingTextView = (TextView) view.findViewById(R.id.trakt_watching_tv);
        directorTextView = (TextView) view.findViewById(R.id.director_tv);
        actorsTextView = (TextView) view.findViewById(R.id.actors_tv);
        tweetsRecyclerView = (RecyclerView) view.findViewById(R.id.tweets_rv);
        tweetsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void updateView() {
        loadImages();

        adultImageView.setVisibility(movie.isAdult() ? View.VISIBLE : View.INVISIBLE);
        titleTextView.setText(movie.getTitle()!=null ? movie.getTitle() : "-");
        yearTextView.setText(movie.getYear()!=null ? movie.getYear() : "-");
        genresTextView.setText(CommonUtils.stringListToString(movie.getGenres()));
        runtimeTextView.setText(movie.getRuntime()!=null ? movie.getRuntime() : "-");
        plotTextView.setText(movie.getPlot()!=null ? movie.getPlot() : "-");
        imdbTextView.setText(movie.getImdbRating()!=null ? movie.getImdbRating()+"" : "-");
        tmdbTextView.setText(movie.getTmdbRating()!=null ? movie.getTmdbRating()+"" : "-");
        rottenTomatoesTextView.setText(movie.getRottenTomatoesRating()!=null ? movie.getRottenTomatoesRating() : "-");
        metacriticsTextView.setText(movie.getMetacriticRating()!=null ? movie.getMetacriticRating() : "-");
        traktWatchingTextView.setText(movie.getCurrentWatchCount()!=null ? movie.getCurrentWatchCount()+" watching" : "0 watching");
        directorTextView.setText(movie.getDirector()!=null ? movie.getDirector() : "-");
        actorsTextView.setText(CommonUtils.stringListToString(movie.getActors()));

        if(movie.getYoutubeLink()!=null) {
            youtubeImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getYoutubeLink()));
                    startActivity(browserIntent);
                }
            });
        }

        updateFavouriteButton();
        favouriteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleMovieFromFavourites();
            }
        });
    }

    private void updateFavouriteButton() {
        SharedObjects<UserModel> sharedObjects = new SharedObjects<UserModel>(getActivity());
        UserModel user = sharedObjects.retrive(Constants.user, UserModel.class);

        favouriteImageButton.setImageDrawable(user.getFavouriteMovies()!=null && user.getFavouriteMovies().contains(movie) ? getResources().getDrawable(R.mipmap.ic_star) : getResources().getDrawable(R.mipmap.ic_star_hollow));
    }

    private void updateTweetsView(List<TweetModel> tweets) {
        TweetsAdapter adapter = new TweetsAdapter(getContext(), tweets);
        tweetsRecyclerView.setAdapter(adapter);
    }


    private void updateImageView(ImageView imageView, Bitmap result) {
        imageView.setImageBitmap(result);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullscreenImageDialog imageDialog = new FullscreenImageDialog();
                imageDialog.setImage(result);
                imageDialog.show(getFragmentManager(), "MovieDetailsDialog");
            }
        });
    }

    private void loadTweets() {
        MovieApi movieApi = new MovieApi();

        try {
            movieApi.apiTweetsMovieIdGetAsync(movie.getTmdbId(), new SimpleCallback<List< TweetModel>> (getFragmentManager()) {
                @Override
                public void onSuccess(List<TweetModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        updateTweetsView(result);
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }


    private void loadImages()  {
        LoaderUtil loader = new LoaderUtil();
        if(movie.getPosterPath()!=null) {
            loader.loadPosterImageFromUrl(movie, new SimpleCallback<Bitmap>() {
                @Override
                public void onSuccess(Bitmap result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        updateImageView(posterImageView, result);
                    });
                }
            });
        }
        if(movie.getCoverPath()!=null) {
            loader.loadCoverImageFromUrl(movie, new SimpleCallback<Bitmap>() {
                @Override
                public void onSuccess(Bitmap result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        updateImageView(coverImageView, result);
                    });
                }
            });
        }
    }

    private void toggleMovieFromFavourites() {
        SharedObjects<UserModel> sharedObjects = new SharedObjects<UserModel>(getContext());
        UserModel user = sharedObjects.retrive(Constants.user, UserModel.class);

        MovieApi movieApi = new MovieApi();
        SimpleCallback<List<MovieModel>> onSuccessCallback = new SimpleCallback<List<MovieModel>>(getFragmentManager()) {
            @Override
            public void onSuccess(List<MovieModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                super.onSuccess(result, statusCode, responseHeaders);
                getActivity().runOnUiThread(() ->{
                    String message = "";
                    if(user.getFavouriteMovies()!=null && user.getFavouriteMovies().contains(movie)) {
                        message = "Movie removed from favourites!";
                        user.getFavouriteMovies().remove(movie);
                    } else {
                        message = "Movie added to favourites!";
                        if(user.getFavouriteMovies()==null ) {
                            user.setFavouriteMovies(new ArrayList<>());
                        }
                        user.getFavouriteMovies().add(movie);
                    }
                    sharedObjects.put(Constants.user, user);
                    updateFavouriteButton();
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                });
            }
        };

        try {
            if(user.getFavouriteMovies()!=null && user.getFavouriteMovies().contains(movie)) {
                movieApi.apiFavouritesUserIdDeleteAsync(user.getId(), movie, onSuccessCallback);
            } else {
                movieApi.apiFavouritesUserIdPostAsync(user.getId(), movie, onSuccessCallback);
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
        
    }

}
