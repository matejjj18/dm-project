package dm.fer.hr.frontend.utils;

import android.support.v4.app.FragmentManager;

import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.dialogs.WaitingLoopDialog;
import io.swagger.client.ApiCallback;
import io.swagger.client.ApiException;

public class SimpleCallback<T> implements ApiCallback<T> {

    private WaitingLoopDialog waitingLoopDialog;

    public SimpleCallback() {

    }

    public SimpleCallback(FragmentManager fragmentManager) {
        waitingLoopDialog = new WaitingLoopDialog();
        waitingLoopDialog.show(fragmentManager, Constants.waitingLoopDialogCallback);
    }

    @Override
    public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
        if (waitingLoopDialog != null) {
            waitingLoopDialog.dismiss();
        }
    }

    @Override
    public void onSuccess(T result, int statusCode, Map<String, List<String>> responseHeaders) {
        if (waitingLoopDialog != null) {
            waitingLoopDialog.dismiss();
        }
    }

    @Override
    public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
        if (done && waitingLoopDialog != null) {
            waitingLoopDialog.dismiss();
        }
    }

    @Override
    public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
        if (done && waitingLoopDialog != null) {
            waitingLoopDialog.dismiss();
        }
    }
}
