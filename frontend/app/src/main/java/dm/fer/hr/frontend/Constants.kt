package dm.fer.hr.frontend

class Constants {
    companion object {
        const val user = "USER_SHARED_OBJECT"
        const val waitingLoopDialogCallback = "WAITING_LOOP_DIALOG_CALLBACK"
        const val movieId = "MOVIE_ID"
        const val pickedInitMovies = "PICKED_INIT_MOVIES"
    }
}