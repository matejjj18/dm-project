package dm.fer.hr.frontend.utils;

import java.util.List;

public class CommonUtils {

    public static String stringListToString(List<String> list) {
        String string = list != null ?  list.get(0) : "-";
        for(int i = 1; list != null && i < list.size(); i++) {
            string += ", " + list.get(i);
        }
        return string;
    }

}
