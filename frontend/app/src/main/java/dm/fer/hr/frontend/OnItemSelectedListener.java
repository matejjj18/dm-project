package dm.fer.hr.frontend;

public interface OnItemSelectedListener<T> {
    void onSelect(T item, boolean isSelected);
}
